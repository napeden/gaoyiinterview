import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import $ from 'jquery';



function ShowResult(state) {
      return <p>The result is: {state.result}</p>;
  }
class Board extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {value: '', result: ''};
        this.handleChange = this.handleChange.bind(this);
        this.get = this.get.bind(this);
    }
    handleChange(event){
        this.setState({value: event.target.value});
    }

    get(){
        $.ajax({
            type:'get',
            url:'ipTrans/' + this.state.value,
            success: function(res) {
                alert( "succuess: " + res );
                this.setState({result: res});
              }.bind(this),
        }).fail(function() {
            alert( "error" );
          })
          .always(function() {
            alert( "complete" );
          });
    }
    render() {
        return (
        <div>
            <div className="board-row">
                <form onSubmit={this.get}>
                    <label>IP:</label>
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                    <input type="submit" value="Submit" />
                </form>
                <fieldset>
                    <label>IP:{this.state.value}</label>
                    <ShowResult result={this.state.result} />
                </fieldset>
            </div>
 
        </div>
        );
    }
}
  
  class App extends React.Component {
    render() {
      return (
        <div className="app">
          <div className="app-board">
            <Board />
          </div>
        </div>
      );
    }
  }
  
  
  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );