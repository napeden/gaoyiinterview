package com.example.iptrans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
// @Controller
@RestController
public class DemoApplication {
	@GetMapping("/ipTrans/{ip}")
	String ipTrans(@PathVariable("ip") String ip) throws Exception {
        boolean isSpace = false;
        boolean isDot = false;
        long temp = 0;
        int k = 0;
        long ipLong = 0;
        int ipLoc = 0;
        
        for(int j = ip.length() - 1; j >= 0; j--){

            if(ip.charAt(j) == ' '){
                if(!isSpace){
                    isSpace = true;
                }
                continue;
            }

            if(ip.charAt(j) <= '9' && ip.charAt(j) >= '0'){
                if(isSpace && (!isDot)){
                    System.out.print("Illegal parameter input");
                    throw new Exception("Illegal parameter input");
                }
                if(isDot || isSpace){
                    isDot = false;
                    isSpace = false;
                }
                temp = (long) (temp + (ip.charAt(j) - '0') * Math.pow(10, k));
                k++;
                continue;
            }

            if(ip.charAt(j) == '.'){
                isDot = true;
                isSpace = false;
                
                if(temp > 255){
                    System.out.print("Input out range the IP rule");
                    throw new Exception("Input out range the IP rule");
                }

                ipLong += temp<<(8*ipLoc);
                k = 0;
                ipLoc++;
                temp = 0;
                continue;
            }
            System.out.print("Illegal parameter input");
            throw new Exception("Illegal parameter input");
        }

        if(ipLoc != 3){
            System.out.print("Illegal parameter input");
            throw new Exception("Illegal parameter input");
        }

        if(temp > 255){
            System.out.print("Input out range the IP rule");
            throw new Exception("Input out range the IP rule");
        }
        ipLong += temp<<(8*ipLoc);
        System.out.println("long: " + ipLong);
        return String.valueOf(ipLong);
	}
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
