package list;

public class IpTrans{
    public long transfer(String ipAddr) throws Exception{
        long ipLong = 0;
        String[] ipStr = ipAddr.split("\\s*\\.\\s*");
        int[] ipInt = new int[4];
        for(int i = 0; i < 4; i++){
            try{
                ipInt[i] = Integer.parseInt(ipStr[i]);
            }catch(NumberFormatException e){
                System.out.print("Illegal parameter input");
                throw new Exception("Illegal parameter input");
            }
            
            ipLong += ((long)ipInt[i]<<(24 - (8*i)));
            // System.out.println("ipLong: " + ipLong);
        }
        System.out.println("ipLong: " + ipLong);
        return ipLong;
    }

    static public void main(String[] args) throws Exception{
        IpTrans trans = new IpTrans();
        trans.transfer("1 72 .   168 .5. 1");
        // trans.transfer("123.45.55.34");
    }
}