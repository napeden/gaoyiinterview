package list;

public class Node{
    String value;
    Node next;

    Node(String val){
        value = val;
        next = null;
    }

    public Node getNext( ){
        return next;
    }

    public String getValue(){
        return value;
    }

    public void setNext(Node node){
        next = node;
    }

    public void setVal(String str){
        value = str;
    }
}