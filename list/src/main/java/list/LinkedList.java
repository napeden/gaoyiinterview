package list;

public class LinkedList implements List{
    private int size = 0;
    private Node head;

    public LinkedList(){
        size = 0;
        head = new Node(null);
    }
    
    public void add(String t){
        Node temp = new Node(t);
        Node current = head;
        while(current.getNext() != null){
            current = current.getNext();
        }
        current.setNext(temp);
        size++;
    }

    public void remove(int index)throws Exception{
        if(index < 0 || index >= size){
            throw new Exception("The index is out of boundary");
        }
        Node cur = head;
        if(cur != null){
            for(int i = 0; i < index; i++){
                cur = cur.getNext();
            }
            cur.setNext(cur.getNext().getNext());
        }
        size--;

    }

    public String get(int index) throws Exception{
        if(index < 0 || index >= size){
            throw new Exception("The index is out of boundary");
        }
        Node cur = head.getNext();
        for(int i = 0; i < index; i++){
            cur = cur.getNext();
        }
        return cur.getValue();
    }

    public void insert(int index,String str){
        if(index < 0 || index > size){
            System.out.println("The index is out of boundary");
            return;
        }
        Node temp = new Node(str);
        Node cur = head;

        if(cur != null){
            for(int i = 0; i < index && cur.getNext() != null; i++){
                cur = cur.getNext();
            }
        }
        temp.setNext(cur.getNext());
        cur.setNext(temp);
        size++;
        return;
    }

    public int length(){
        return size;
    }

    public void clear(){
        if(size == 0){
            return;
        }
        for(Node cur = head; cur != null; ){
            Node next = cur.getNext();
            cur.setVal(null);
            cur.setNext(null);
            cur = next;
        }

        size = 0;
        head = new Node(null);
    }
    


}