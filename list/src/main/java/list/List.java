package list;

public interface List{

    /**
    * 新增 个元素
    * @param t
    */
    void add(String t);
    /**
    * 移除指定位置的元素
    * @param index 元素位置
    */

    void remove(int index)throws Exception;
    /**
    * 返回指定位置的元素
    * @param index 元素位置
    * @return 元素
    */

    String get(int index)throws Exception;

    /**
    * 在指定位置插 元素
    * @param index 插 位置
    * @param String 被插 的元素
    */
    void insert(int index,String str);

    /**
     * return Lenth of List
     * @ return 
     */
    int length();

    /**
     * clear List
     */
    void clear();

}