package list;

public class ArrayList implements List{

    private final int LEN = 8;
    private int size;
    private String[] elements;

    public ArrayList(){
        size = 0;
        elements = new String[LEN];
    }
    
    public void add(String t){
        if(size >= elements.length){
            expandeSize();
        }
        elements[size++] = t;
        return;
        
    }
    
    private void expandeSize(){
        String[] a = new String[elements.length*2];
        for(int i = 0; i < elements.length; i++){
            a[i] = elements[i];
        }
        elements = a;
    }


    public void remove(int index) throws Exception{
        if(index < 0 || index >= size){
            throw new Exception("The index is out of boundary");
        }
        
        for(int j = index; j < size - 1; j++){
            elements[j] = elements[j+1];
        }
        elements[--size] = null;
        return;
    }

    public String get(int index)throws Exception{
        if(index < 0 || index >= size){
            throw new Exception("The index is out of boundary");
        }
        return elements[index];
    }

    public void insert(int index, String str){
        if(size >= elements.length){
            expandeSize();
        }
        for(int j = size; j > index; j--){
            elements[j] = elements[j-1];

        }
        elements[index] = str;
        size++;
        return;
    }

    public int length(){
        return size;
    }
     
    public void clear(){
        String[] a = new String[LEN];
        elements = a;
        size = 0;
    }

}