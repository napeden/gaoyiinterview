package list;

public class test{

    public static void main(String[] args) throws Exception{
        // ArrayList testList = new ArrayList();
        LinkedList testList = new LinkedList();
        testList.add("1");
        testList.add("2");
        testList.add("3");
        testList.add("4");
        testList.add("5");
        testList.add("6");
        testList.add("7");
        testList.add("8");
        testList.add("9");
        testList.add("10");
        System.out.println("Print: " + testList.length());
        System.out.println("get(0): " + testList.get(0));
        testList.remove(0);
        testList.remove(8);
        System.out.println("get(0): " + testList.get(0));
        System.out.println("length: " + testList.length());
        System.out.println("get(7): " + testList.get(7));
        testList.clear();
        System.out.println("Print: " + testList.length());
        testList.add("1");
        testList.add("2");
        System.out.println("get(0): " + testList.get(0));
        testList.insert(0,"11");
        System.out.println("get(0): " + testList.get(0));
        System.out.println("get(1): " + testList.get(1));
        System.out.println("get(2): " + testList.get(2));
        System.out.println("Print: " + testList.length());

        // System.out.println("get(7): " + testList.get(7));
    }
}