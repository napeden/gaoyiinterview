# gaoyiInterview

## 第一题 list

代码可见list目录中，测试代码在test.java中。


## 第二题 IPtrans

### BackEnd

基本后端逻辑代码在DemoApplication中。
运行：

```term
mvn spring-boot:run
```

就可以启动后端服务。
测试：

```term
curl localhost:8080/ipTrans/255.1.1.1
4278255873
curl localhost:8080/ipTrans/255.1%20.1%20.1
4278255873
curl localhost:8080/ipTrans/172.168.5.1
2896692481
```

## front-end

代码在 ipTrans/my-app中。
具体运行：

1. downloader the node_modules. `npm install `
2. build, run the cmd: `npm run build`
3. mv the dist/main.js into ../src/main/resources/static/main.js. Cmd: `cp dist/main.js ../src/main/resources/static/main.js`

在这之后，运行`mvn spring-boot:run`,访问：http://127.0.0.1:8080/?，即可见到页面。
